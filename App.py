from datetime import datetime
from os import makedirs, path
from time import sleep, time
from typing import List

import picamera

camera: picamera.PiCamera = picamera.PiCamera()
camera.rotation = 180
log: 'Log'
settings: 'Settings'


class MetadataOverflow(Exception):
    pass


def callback_camera(ag: 'Agent', _user_data, msg: 'Message'):
    try:
        msg_str: str = msg.payload.decode()
        log.write(f"CAMERA: {msg_str.upper()}")

        if not msg_str == "capture" and not msg_str.startswith("record"):
            log.write("UNKNOWN COMMAND")
            return

        date: str = datetime.now().isoformat()
        file: str = settings["folders"]["data"]
        metadata: List[str] = []

        if msg_str == "capture":
            metadata = [settings["camera"]["image_topic"], "jpg", date, ""]
            file = path.join(file, metadata[0], f"{date}.{metadata[1]}")
            camera.capture(file)
        elif msg_str.startswith("record"):
            metadata = [settings["camera"]["video_topic"], "h264", date, ""]
            file = path.join(file, metadata[0], f"{date}.{metadata[1]}")
            record_time: float = float(msg_str[6:]) if msg_str[6:] else 5
            t1 = time()
            camera.start_recording(file)
            while record_time > (time() - t1):
                sleep(0.01)
            camera.stop_recording()
            record_time = time() - t1
            metadata[3] = f"{record_time:.2f}"

        str_metadata: str = "\x10".join(metadata)
        if len(str_metadata) > 127:
            raise MetadataOverflow(f"{len(str_metadata):x}>{127:x}")
        bin_metadata: bytes = f"{chr(len(str_metadata))}{str_metadata}".encode()
        bin_file: bytes
        with open(file, "rb") as f:
            bin_file = f.read()
        bin_data: bytes = bin_metadata + bin_file

        while not ag.is_connected():
            ag.loop(1)
            sleep(0.5)

        ag.publish(metadata[0], bin_data)
    except (BaseException, Exception) as err:
        log.write(f"CAMERA ERROR:{repr(err)}")
        ag.publish("console", f"CAMERA ERROR:{repr(err)}")


def app(settings_arg: 'Settings', log_arg: 'Log', agent: 'Agent'):
    global settings
    global log

    settings = settings_arg
    log = log_arg

    makedirs(path.join(settings["folders"]["data"], settings["camera"]["image_topic"]), exist_ok=True)
    makedirs(path.join(settings["folders"]["data"], settings["camera"]["video_topic"]), exist_ok=True)

    agent.subscribe(settings["camera"]["control_topic"])
    agent.message_callback_add(settings["camera"]["control_topic"], callback_camera)

    while True:
        agent.loop(1)
